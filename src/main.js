import Vue from 'vue'
import Vuetify from 'vuetify'
import 'babel-polyfill'
import App from './App.vue'

// styles
import 'vuetify/dist/vuetify.min.css'

Vue.use(Vuetify)

new Vue({
  el: '#app',
  render: h => h(App)
})
